<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class GenericController extends Controller {
	public function index(Request $request) {
		try {
			if ($view = view('pages.' . Route::currentRouteName())) return $view;
		} catch (Exception $e) {
			abort(404);
		}
	}
}