var pem = require('pem');

pem.createPrivateKey(4096,{cipher:'aes128'},function(err,key) {
	if (err) {
		console.log(err);
		process.exit();
	}

	pem.getPublicKey(key.key,function(err,pub) {
		console.log(pub);
	});

	console.log(key);
});
