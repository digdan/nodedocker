//Builder
//
//	Builds node-docker

var 
	fs=require('fs'),
	ini = require('ini'),
	Docker = require('dockerode')
	Builder = require('./objects/builder.js')

var
	config = ini.parse(fs.readFileSync('../config.ini','utf-8')),
	builder = new Builder(config);

builder.allocateIp(config.servers['grunt'],function(err,targetIp) {
	targetPort = builder.allocatePort(targetIp,function(err,targetPort) {
		var portMin = targetPort;
		var portMax = targetPort + config.general.portCount;
		var build = builder.build(targetIp,portMin,'default','key');
	});
});
