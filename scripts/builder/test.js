//Builder
//
//	Builds node-docker

var 
	fs=require('fs'),
	ini = require('ini'),
	mongoose = require('mongoose'),

	modelServer = mongoose.model('Server', require('./objects/server.js'));


var
	config = ini.parse(fs.readFileSync('../config.ini','utf-8'));

mongoose.connect(config.mongo.dsn);

var server = new modelServer();
config.servers['grunt'].forEach(function(entry) {
	server.connect(entry,config.general.defaultPort,function(err) {
		if (err) throw err;
		var basePort = server.findBasePort();
	});
});
