var Docker = require('dockerode');
var mongoose = require('mongoose');

var portMark = new mongoose.Schema({
	container_id:mongoose.Schema.Types.ObjectId,
	start:Number,
	span:Number
});
var schema = mongoose.Schema({
	_id : mongoose.Schema.Types.ObjectId,
	ip : String,
	name : String,
	containers : [mongoose.Schema.Types.ObjectId],
	portMap : [portMark]			
});

schema.docker = new Docker();

schema.methods.connect = function (ip,port,callback) {
	this.docker = new Docker({host:ip, port:port});
	callback();
}

schema.methods.findBasePort = function() {
	//Find Base Pot by iterating through PortMarks on server
	console.log(this.portMap.length);
}

schema.methods.getDocker = function() {
	return this.docker;
}

schema.methods.listContainers = function(callback) {
	this.docker.listContainers(function (err,containers) {
		if (err) throw err;
		callback(containers);
	});	
}

module.exports = schema;
