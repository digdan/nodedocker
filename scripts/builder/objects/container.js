var Docker = require('dockerode');
var mongoose = require('mongoose');

module.exports = function() { 
	var schema = mongoose.Schema({
		_id : mongoose.Schema.Types.ObjectId,
		server_id : mongoose.Schema.Types.ObjectId,
		user_id : mongoose.Schema.Types.ObjectId,
		base_port : Number,
		container_id : String,
		name : String,
		size : Number,
		created : Date,
		updated : Date
	});

	schema.methods.build = function(serverName) {
		//
	}

	schema.methods.getBasePort = function() {
		//Find Base Pot by iterating through PortMarks on server
		console.log(this.portMap);
	}

	return schema;
}
