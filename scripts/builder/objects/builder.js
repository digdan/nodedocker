//Builder Object

	var Docker = require('dockerode');
	var config;	

	this.allocateIp = function(servers,callback) {
		//Iterate through hosts querying number of inhabitants.
		var config = this.config;
		servers.forEach(function(entry) {
			var docker = new Docker({host:entry,port:config.general.defaultPort});	
			docker.listContainers(function(err,containers) {
				if (err) {
					console.log('Error:'+err);
					callback(err);
				}
				if (containers.length < maxContainers) callback(null,entry);
			});
		});
	}

	this.allocatePort = function(serverIp,callback) {
		var config = this.config;
		var docker = new Docker({host:serverIp,port:config.general.defaultPort});
		var highest=0;
		docker.listContainers(function (err, containers) {
			if (containers == null) {
				callback(highest);
			} else {	
				containers.forEach(function (containerInfo) {
					docker.getContainer(containerInfo.Id).inspect(function (err,data) {
						if (err) {
							console.log('Container Error : '+err);
							callback(err);
						} else {
							console.log('Container Info : ');
							console.log(data);
						}
					});
				});
				callback(null,highest);
			}
		});
	}

	this.build = function(serverIp,basePort,targetImage,key) {
		console.log('Build triggered for '+serverIP+':'+basePort+' <- '+targetImage+' using key '+key+'\n');
	}
