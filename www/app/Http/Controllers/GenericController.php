<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class GenericController extends Controller {
	public function index(Request $request) {
		try {
			if (view()->exists('pages.'. Route::currentRouteName() )) {
				return view( 'pages.'. Route::currentRouteName() );
			} else {
				abort(404);
			}
		} catch (Exception $e) {
			abort(404);
		}
	}
}