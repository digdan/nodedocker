@extends('layouts.default')
@section('content')
<DIV class='text-center panel panel-default'>
    <H1>Sign Up</H1>
    <form method="POST" action="{{ URL::to('register') }}">
        {!! csrf_field() !!}

        <div class='form-group'>
            Name
            <input type="text" name="name" value="{{ old('name') }}" style='form-control'>
        </div>

        <div class='form-group'>
            Email
            <input type="email" name="email" value="{{ old('email') }}" style='form-control'>
        </div>

        <div class='form-group'>
            Password
            <input type="password" name="password" style='form-control'>
        </div>

        <div class='form-group'>
            Confirm Password
            <input type="password" name="password_confirmation" style='form-control'>
        </div>

        <div>
            <button type="submit" class='btn btn-default'>Register</button>
        </div>
    </form>
</DIV>
@stop