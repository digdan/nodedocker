@extends('layouts.default')
@section('content')
    <div class='row'>
        <div class='col-md-6'>
            <H1>Simple container hosting</H1>
            <H2>Built for developers by developers</H2>
            <P>Deploy your services and scripts instantly into our cloud or lease your own competitively priced cloud server.</P>
            <form method="POST" action="/register">
                {!! csrf_field() !!}
                <div class='form-group'>
                    <label for="signupEmail">Email Address</label>
                    <input type="text" name="signupEmail" class="form-control">
                </div>
                <div class='form-group'>
                    <label for="signupPass">Password</label>
                    <input type="password" name="signupPass" class="form-control">
                </div>
                <div class='form-group'>
                    <input type="submit" name="btn" class="btn btn-block btn-lg btn-primary" value="Create Account">
                </div>
            </form>
        </div>
        <div class='col-md-6'> <img src="/images/front-block.png" style="width:100%"> </div>
    </div>
    <HR>
    <div class='row'>
        <div class='col-md-12'>
            <img src="http://lorempixel.com/400/200/abstract" style="float:left;margin:20px">
            <H2>What is Container Hosting?</H2>
            <P>Just as the world of server virtualizations changed the landscape of the Internet hosting realm, containers are posed to do the same to the applications on the servers themselves.</P>
            <P>Containers are packaged resources that are utilized by various scripts. These packages can easily be moved from one host to another without regard to their Operating System.</P>
            <P>Now your services are completely self contained and can be distributed, moved, swarmed, copied, sold without further regards to the infrastructure and operating systems they are hosted on.</P>
        </div>
    </div>

@stop