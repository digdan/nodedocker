<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body>

<div id="spark-app" v-cloak>
    @if (Auth::check())
        @include('includes.header-authed')
    @else
        @include('includes.header')
    @endif

    <div class="container">
        <div id="main" class="row">
            @yield('content')
        </div>

        <footer class="row">
            @include('includes.footer')
        </footer>
    </div>
</div>
<script src="/js/app.js"></script>
</body>
</html>